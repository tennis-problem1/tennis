class tennis_match:
    def __init__(self, name):
        self.name = name
        self.points = 0
        self.games = 0
        self.sets = 0
        self.match = 0
    
def point_calculator(s: str, A, B):
    for win in s:
        if win == 'A':
            A.points = next_point(A.points)
        else:
            B.points = next_point(B.points)
        end_points(A, B)
        end_games(A,B)
        end_sets(A,B)
        
            
def next_point(n: int) -> int:
    if n == 0:
        return 15
    elif n == 15: 
        return 30
    elif n == 30:
        return 40
    else: 
        return n+10

def end_points(A, B):
    pA = A.points
    pB = B.points
    dif = abs(pA - pB)
    if (pA > 40 or pB > 40) and dif >= 20: 
        if pA > pB:
            A.games += 1
        else:
            B.games += 1
        A.points = 0
        B.points = 0

def end_games(A,B):
    gA = A.games
    gB = B.games
    dif = abs(gA - gB)
    if (gA >= 6 or gB >= 6 ) and dif >= 2:
        if gA > gB:
            A.sets += 1
        else:
            B.sets += 1
        A.games = 0
        B.games = 0
        

def end_sets(A,B):
    sA = A.sets
    sB = B.sets
    if sA >= 2 or sB >= 2:
        if sA > sB:
            A.match = 1
        else:
            B.match = 1


            